let snake = [{ x: 150, y: 150 },
{ x: 140, y: 150 },
{ x: 130, y: 150 },
{ x: 120, y: 150 }
]
// user's score

let score = 0;

//when is set to true snake is changing direction
let changingDirection = false;


// food X-coordinate

let foodX;

// food Y-coordinate

let foodY;

// horisontal velocity

let dx = 10;

// vertical velocity

let dy = 0;

// Get the canvas element

const gameCanvas = document.getElementById('gameCanvas');

// Return a 2d drawing context

const ctx = gameCanvas.getContext('2d');

const totalScore = document.getElementById('score');

createFood();
main();
const button = document.addEventListener('keydown', changeDirection);
// Create the canvas

function clearCanvas() {
 ctx.fillStyle = 'white';
 ctx.strokeStyle = 'black';
 ctx.fillRect(0, 0, gameCanvas.width, gameCanvas.height);
 ctx.strokeRect(0, 0, gameCanvas.width, gameCanvas.height);
}

// Move the snake automatically

function main() {
 if (gameOver()) return;
 setTimeout(function onTick() {
  changingDirection = false;
  clearCanvas();
  drawFood();
  updateSnake();
  drawSnake();
  main();
 }, 60)
};

// Create Snake



// Draw snake on the canvas
drawSnake();
function drawSnakePart(snakePart) {
 ctx.fillStyle = 'lightgreen';
 ctx.strokeStyle = 'darkgreen';
 ctx.fillRect(snakePart.x, snakePart.y, 10, 10);
 ctx.strokeRect(snakePart.x, snakePart.y, 10, 10);
}

function drawSnake() {
 snake.forEach(drawSnakePart);
}

// Enable snake to move horizontaly

function updateSnake() {
 const head = { x: snake[0].x + dx, y: snake[0].y + dy };
 snake.unshift(head);
 const eatFood = snake[0].x === foodX && snake[0].y === foodY;

 // Tracking score 


 if (eatFood) {
  score += 1;
  totalScore.innerHTML = score;
  createFood();
 } else { snake.pop(); }
}



// Changing direction 

function changeDirection(e) {
 const left = 37;
 const right = 39;
 const up = 38;
 const down = 40;
 if (changingDirection) return;
 changingDirection = true;
 const keyPressed = e.keyCode;
 const goLeft = dx === -10;
 const goRight = dx === 10;
 const goUp = dy === -10;
 const goDown = dy === 10;
 if (keyPressed === left && !goRight) {
  dx = -10;
  dy = 0;
 }
 if (keyPressed === right && !goLeft) {
  dx = 10;
  dy = 0;
 }
 if (keyPressed === up && !goDown) {
  dx = 0;
  dy = -10;
 }
 if (keyPressed === down && !goUp) {
  dx = 0;
  dy = 10;
 }
}

// Generate random number


function randomCoor(max, min) {
 return Math.round((Math.random() * (max - min) + min) / 10) * 10;
}

// Create food

function createFood() {
 foodX = randomCoor(0, gameCanvas.width - 10);
 foodY = randomCoor(0, gameCanvas.height - 10);
 snake.forEach(function isFoodOnSnake(part) {
  const foodIsOnSnake = part.x === foodX && part.y === foodY;
  if (foodIsOnSnake)
   createFood();
 })
}

// Draw food

function drawFood() {
 ctx.fillStyle = "red";
 ctx.strokeStyle = 'darkred';
 ctx.fillRect(foodX, foodY, 10, 10);
 ctx.strokeRect(foodX, foodY, 10, 10);
}

// End game

function gameOver() {
 for (let i = 4; i < snake.length; i++) {
  const didTouch = snake[i].x === snake[0].x && snake[i].y === snake[0].y;
  if (didTouch) return true;
 }
 const hitLeftWall = snake[0].x < 0;
 const hitRightWall = snake[0].x >= gameCanvas.width - 10;
 const hitTopWall = snake[0].y < 0;
 const hitBottomWall = snake[0].y >= gameCanvas.height - 10;
 return hitLeftWall || hitRightWall || hitTopWall || hitBottomWall;
}