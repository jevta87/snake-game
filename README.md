# Basic Snake-game
I've made an own version of the classic Snake game for the purposes of testing skills. It's made by following instructions from the task.

## Project files

index.html - The main page of the project which user uses to play the game.

## Tools/Languages used

* Vannila JS, HTML, CSS
* GIT

## Workflow 

I've broken a problem into smaller steps, and at the end bind them all together.

* Creating the canvas
* Give the canvas some style
* Creating and drawing snake
* Enabling snake to move/automatically
* Changing snake direction
* Generate food for the snake
* Growing the snake
* Keeping track of the score
* Check end of the game
* Bind it all together

## Author 

* Milovan Jevtic

## License

You are free to use this game, and code as well. Any kind of feedback is welcome.

## Additional info

I've tried to optimize my code as much as I can in order to reach 60fps, but failed unfortunatelly. I hope get some useful tips to fix it.






